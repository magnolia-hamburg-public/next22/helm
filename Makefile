RELEASE=next
VALUES_FILE=values.yaml
CHART_NAME=mironet/magnolia-helm
CHART_VERSION=1.4.5-rc8

# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help

values: ## Show generated yaml resources and values.
	helm install --dry-run --debug -f $(VALUES_FILE) --version $(CHART_VERSION) --generate-name $(CHART_NAME) -n $(RELEASE)

clean: ## Clean up environment.
	helm del $(RELEASE) -n $(RELEASE)

clean-pvc: ## Clean disks (PVCs) too.
	kubectl get persistentvolumeclaims -n $(RELEASE) -l 'release=$(RELEASE)' -o json | kubectl delete -f -

install: ## Install helm chart on k8s.
	helm upgrade --install $(RELEASE) $(CHART_NAME) --version $(CHART_VERSION) --create-namespace -n $(RELEASE) -f $(VALUES_FILE)

upgrade: ## Upgrade locally deployed release.
	helm upgrade $(RELEASE) $(CHART_NAME) --version $(CHART_VERSION) -n $(RELEASE) --reuse-values -f $(VALUES_FILE)

test: ## Start helm tests.
	helm test --logs $(RELEASE) -n $(RELEASE)

template: ## Template out, do not send to k8s.
	helm template -f $(VALUES_FILE) $(CHART_NAME) --version $(CHART_VERSION) -n $(RELEASE)
